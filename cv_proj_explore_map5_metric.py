# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

from fastai2.metrics import top_k_accuracy
import torch
import numpy as np


# +
# https://www.kaggle.com/pestipeti/explanation-of-map5-scoring-metric/

# +
# https://www.kaggle.com/heye0507/prepare-databunch-with-fastai-1-0

# +
# https://forums.fast.ai/t/add-mapk-metric-to-fast-ai-library/38255
# -

def mapk(preds,targs,k=5):
    def singlemap(pred,label,k=5):
        try:
            return 1/ ((pred[:k] == label).nonzero().item()+1)
        except ValueError:
            return 0.0
    
    batchpred = preds.sort(descending=True)[1] #batchsize * classes
    return torch.tensor(np.mean([singlemap(p,l,k) for l,p in zip(targs,batchpred)]))

# + {"tags": ["active-ipynb"]}
# mapk(preds=torch.Tensor([[1,0.8,0.6,0.4,0.2]]), targs=torch.LongTensor([0]), k=1)

# + {"tags": ["active-ipynb"]}
# mapk(preds=torch.Tensor([[0.8,1.0,0.6,0.4,0.2]]), targs=torch.LongTensor([0]), k=1)

# + {"tags": ["active-ipynb"]}
# mapk(preds=torch.Tensor([[0.8,1.0,0.6,0.4,0.2]]), targs=torch.LongTensor([0]), k=2)

# + {"tags": ["active-ipynb"]}
# mapk(preds=torch.Tensor([[0.6,0.8,1.0,0.4,0.2]]), targs=torch.LongTensor([0]), k=3)
