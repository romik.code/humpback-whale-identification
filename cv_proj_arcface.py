# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# ### Only ArcFace implementation, copied from Kaggle Kernel "Arcface_humpback_Customhead_FastAI_score919"
# https://www.kaggle.com/jaideepvalani/arcface-humpback-customhead-fastai-score919/output?scriptVersionId=11974364

from cv_proj_imports import *
from torch import nn
import math


class ArcMarginProduct(nn.Module):
    r"""Implement of large margin arc distance: :
        Args:
            in_features: size of each input sample
            out_features: size of each output sample
            s: norm of input feature
            m: margin
            cos(theta + m)
        """
    def __init__(self, in_features, out_features):
        super(ArcMarginProduct, self).__init__()
        self.weight = nn.Parameter(torch.FloatTensor(out_features, in_features))
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)
    
    def forward(self, features):
        cosine = F.linear(F.normalize(features), F.normalize(self.weight))
        return cosine


# + {"tags": ["active-ipynb"]}
# # test
#
# arc_margic_product = ArcMarginProduct(1024, 1078)
#
# #features = torch.rand(1024*16, dtype=torch.double).reshape(16, 1024)
# features = torch.rand(1024*16).reshape(16, 1024)
#
# arc_margic_product.forward(features)
# -

class Customhead(nn.Module):
    r"""Implement of large margin arc distance: :
        Args:
            in_features: size of each input sample
            out_features: size of each output sample
            s: norm of input feature
            m: margin
            cos(theta + m)
        """
    def __init__(self, in_features, out_features, nc=1024):
        super(Customhead, self).__init__()
        self.head=create_head(nf=in_features, nc=nc,lin_ftrs=[2048],  ps=0.5, bn_final=False) # 1024 no of classes
        self.arc_margin=ArcMarginProduct(nc,out_features)


    def forward(self, features):
        x=self.head(features)
        cosine = self.arc_margin(x)
        cosine = cosine.clamp(-1, 1)
        return cosine


class ArcFaceLoss(nn.modules.Module):
    def __init__(self,s=30.0,m=0.5):
        super(ArcFaceLoss, self).__init__()
        self.classify_loss = nn.CrossEntropyLoss()
        self.s = s
        self.easy_margin = False
        self.cos_m = math.cos(m) 
        self.sin_m = math.sin(m) 
        self.th = math.cos(math.pi - m)
        self.mm = math.sin(math.pi - m) * m

    def forward(self, inputs, labels, epoch=0,reduction=None):
        cosine = inputs
        sine = torch.sqrt(1.0 - torch.pow(cosine, 2))
        phi = cosine * self.cos_m - sine * self.sin_m
        if self.easy_margin:
            phi = torch.where(cosine > 0, phi, cosine)
        else:
            phi = torch.where(cosine > self.th, phi, cosine - self.mm)

        one_hot = torch.zeros(cosine.size(), device='cuda')
        one_hot.scatter_(1, labels.view(-1, 1).long(), 1)
        # -------------torch.where(out_i = {x_i if condition_i else y_i) -------------
        output = (one_hot * phi) + ((1.0 - one_hot) * cosine)
        output *= self.s
        loss1 = self.classify_loss(output, labels) # this is as per paper what is missing here is centralized features
        loss2 = self.classify_loss(cosine, labels)
        gamma=1
        loss=(loss1+gamma*loss2)/(1+gamma)
        return loss
