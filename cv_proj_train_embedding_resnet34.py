# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# https://ipython.org/ipython-doc/3/config/extensions/autoreload.html
# %load_ext autoreload
# %autoreload 2

from cv_proj_imports import *
from cv_proj_consts import *
from cv_proj_utils import *
from cv_proj_explore_map5_metric import *
from cv_proj_trainval_split import *

# +
# transformations
# we get the filenames as input, and output:
# x as the image
# y as the whale name
tfms_x = [PILImage.create]
tfms_y = [filename_label, lambda o: train_labels_dict[o], Categorize()]
tfms = [tfms_x, tfms_y]

ds_img_tfms = [ToTensor(), Resize(224)]

dl_tfms = [Cuda(), ByteToFloatTensor(), Normalize(*imagenet_stats)]
dl_tfms.extend(aug_transforms(
    do_flip=False, # flipping makes it a different whale identity!
    flip_vert=False,
    max_rotate=30.,
    max_zoom=1.,
    max_lighting=0.4,
    max_warp=0.1,
    p_affine=0.75,
    p_lighting=0.75,
    pad_mode=PadMode.Border
))
# -

trainval_splits = get_trainval_splits()
train_ds = DataSource(train_image_files, tfms, splits=trainval_splits)
train_db = train_ds.databunch(after_item=ds_img_tfms, after_batch=dl_tfms, bs=32, num_workers=0)

opt_func = partial(Adam, lr=5e-3, wd=0.01)

# +
from fastai2.vision.learner import _resnet_split

learn = cnn_learner(train_db, models.resnet34, loss_func=CrossEntropyLossFlat(), cut=-2, splitter = _resnet_split, opt_func=opt_func, metrics=[error_rate, mapk])
# -

chosen_lr = find_best_lr(learn); f"Chosen LR: {chosen_lr}"


class FixedReduceLROnPlateau(TrackerCallback):
    "A `TrackerCallback` that reduces learning rate when a metric has stopped improving."
    def __init__(self, monitor='valid_loss', comp=None, min_delta=0., patience=1, factor=10.):
        super().__init__(monitor=monitor, comp=comp, min_delta=min_delta)
        self.patience,self.factor = patience,factor

    def begin_fit(self): self.wait = 0; super().begin_fit()
    def after_epoch(self):
        "Compare the value monitored to its best score and maybe stop training."
        super().after_epoch()
        if self.new_best: self.wait = 0
        else:
            self.wait += 1
            if self.wait >= self.patience:
                for h in self.opt.hypers:
                    old_lr = h['lr']
                    new_lr = old_lr / self.factor
                    h['lr'] = new_lr
                    print(f'Epoch {self.epoch}: reducing lr from {old_lr} to {new_lr} in {h} with hypers {str(self.opt.hypers)}')
                self.wait = 0                


# +
learn_cbs = [
    EarlyStoppingCallback(monitor='mapk', patience=20),
    SaveModelCallback(monitor='mapk', fname='embedding_resnet34_20191027_v2_best'),
    FixedReduceLROnPlateau(monitor='mapk', patience=5)
]

learn.fit(500, lr=chosen_lr/10., cbs=learn_cbs)
# -

learn.recorder.plot_loss()

learn.unfreeze()

chosen_lr = find_best_lr(learn); f"Chosen LR: {chosen_lr}"

# +
learn_cbs = [
    EarlyStoppingCallback(monitor='mapk', patience=20),
    SaveModelCallback(monitor='mapk', fname='embedding_resnet34_20191027_v2_unfreeze_best'),
    FixedReduceLROnPlateau(monitor='mapk', patience=5)
]

learn.fit(500, lr=chosen_lr/10., cbs=learn_cbs)
# -

learn.model
