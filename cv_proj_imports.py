# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Imports
#
# ## fastai v2
# Using fastai v2, which is the new version of fastai library, being developed these days (Sep 2019).
# The library was developed to simplify training of neural networks and apply modern best practices.
# Version 2 was developed from scratch, following the online MOOC course "Deep Learning from the Foundation": https://course.fast.ai/part2
# It adds many useful wrappers for Python and PyTorch, to make them more usable and reduce boilerplate code.
#
# The development version of fastai is installed from here:
# https://github.com/fastai/fastai_dev
#
# Then *symlink* from:
# `~/github/fastai_dev/dev/local/`
# to:
# `~/anaconda3/envs/fastai/lib/python3.7/site-packages/`
#
#
# To install the environment, use:
#
# `conda install -c anaconda -c pytorch -c conda-forge -c fastai pytorch-gpu jupyter jupyterlab nodejs torchvision matplotlib pandas requests pyyaml fastprogress pillow pip scikit-learn imageio scikit-image scipy spacy jupyter_contrib_nbextensions jupyter_nbextensions_configurator typeguard kaggle imagehash`

# fastai uses it's own code style guide: https://docs.fast.ai/dev/style.html
# and has abbreviations for all common terms: https://docs.fast.ai/dev/abbr.html

# fastai v2 index for forums topics that explain basic concepts:
# https://forums.fast.ai/t/fastai-v2-read-this-before-posting-please/53517
#
# fastai v2 is work in progress, the most clear documentation at the moment is in [code walk-thrus](https://forums.fast.ai/t/fastai-v2-daily-code-walk-thrus/53839) (videos and their notes)
#
# 1. [Library concepts, basic usage examples](https://forums.fast.ai/t/fastai-v2-code-walk-thru-1/53940)
# 2. [End to end tutorial: Split, Label, Category, Transform](https://forums.fast.ai/t/fastai-v2-code-walk-thru-2/53978)
# 3. [Transforms, Databunch, DataLoader](https://forums.fast.ai/t/fastai-v2-code-walk-thru-3/54067)
# 4. [Transform function with encodes/decodes](https://forums.fast.ai/t/fastai-v2-code-walk-thru-4/54137)
# 5. [Digging Deep in Transforms](https://forums.fast.ai/t/fastai-v2-code-walk-thru-5/54405)
# 6. [TypeDispatch, Data Pipeline, Data Source](https://forums.fast.ai/t/fastai-v2-code-walk-thru-6/54775)
# 7. https://forums.fast.ai/t/fastai-v2-code-walk-thru-7/54924
# 8. https://forums.fast.ai/t/fastai-v2-code-walk-thru-8/55068
# 9. https://forums.fast.ai/t/fastai-v2-code-walk-thru-9/55234
# 10. https://forums.fast.ai/t/fastai-v2-code-walk-thru-10/55324

# Imports based on Transfer Learning tutorial:
# https://github.com/fastai/fastai_dev/blob/master/dev/23_tutorial_transfer_learning.ipynb

from fastai2.torch_basics import *
from fastai2.core import *
from fastai2.layers import *
from fastai2.data.all import *
from fastai2.optimizer import *
from fastai2.learner import *
from fastai2.vision.core import *
from fastai2.vision.augment import *
from fastai2.vision.learner import *
from fastai2.metrics import *
from fastai2.data.block import *
from fastai2.callback.tracker import *

# ## Python Libraries
#
# - OpenCV (cv2) for image manipulation
# - ImageHash for finding duplicate images
# - TQDM for progress bar of long operations
# - MatPlotLib for graphs
# - SeaBorn for better looking MatPlotLib graphs
# - PIL for image manipulation and metadata extractions
# - PandaSQL for querying Dataframes using SQL

import cv2
import imagehash
from tqdm.autonotebook import tqdm
import PIL
from PIL import ExifTags, ImageOps
from operator import itemgetter
from collections import defaultdict
import pickle
from pandasql import sqldf
pysqldf = lambda q: sqldf(q, globals())

from efficientnet_pytorch import EfficientNet

# ## PyTorch Models

# +
# https://pytorch.org/docs/stable/torchvision/models.html

from torchvision.models import resnet34
from torchvision.models import vgg16_bn
from fastai2.vision.models.xresnet import xresnet34

from torchvision import models

cv_transfer_models = [
    models.resnet18,
    models.alexnet,
    models.squeezenet1_0,
    models.vgg16,
    models.densenet161,
    models.inception_v3,
    models.googlenet,
    models.shufflenet_v2_x1_0,
    models.mobilenet_v2,
    models.resnext50_32x4d,
    models.wide_resnet50_2,
    models.mnasnet1_0
]

# + {"tags": ["active-ipynb"]}
# torch.backends.cudnn.enabled

# + {"tags": ["active-ipynb"]}
# torch.backends.cudnn.version()
# -

# ## Matplotlib

import matplotlib.pyplot as plt
import seaborn as sns

# %matplotlib inline
plt.rcParams['figure.figsize'] = [12, 9]
