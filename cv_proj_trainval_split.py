from cv_proj_imports import *
from cv_proj_consts import *

def get_trainval_splits():
    whale_images_for_trainval_split = sqldf("""
    select
        Id as whale_id, count(Id) as image_count, group_concat(Image) as images
    from
        train_labels_df
    where
        Id != 'new_whale'
    group by
        Id
    having
       image_count>=4
    """)

    print(f'We will use {len(whale_images_for_trainval_split)} whales for initial train/val')

    train_labels_image_to_idx = {row[0]: idx for idx, row in train_labels_df.iterrows()}

    randgen = random.Random(42)

    images_train_idx_allrows = []
    images_val_idx_allrows = []

    for idx, row in whale_images_for_trainval_split.iterrows():
        # data from above query
        whale_id = str(row[0])
        image_count = int(row[1])
        images = str(row[2]).split(',')

        # split images into train/val
        randgen.shuffle(images)
        trainval_cut_idx = int(image_count * 0.8)

        # at leat one validation image
        if trainval_cut_idx>=image_count:
            trainval_cut_idx = image_count-1

        images_train = images[:trainval_cut_idx]
        images_val = images[trainval_cut_idx:]

        if len(images_train)>0:
            images_train_idx = itemgetter(*images_train)(train_labels_image_to_idx)    
            if type(images_train_idx) is not tuple: images_train_idx = (images_train_idx,)
            images_train_idx_allrows.extend(images_train_idx)

        if len(images_val)>0:
            images_val_idx = itemgetter(*images_val)(train_labels_image_to_idx)
            if type(images_val_idx) is not tuple: images_val_idx = (images_val_idx,)
            images_val_idx_allrows.extend(images_val_idx)

    return [images_train_idx_allrows, images_val_idx_allrows]