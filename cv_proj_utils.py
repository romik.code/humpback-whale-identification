from cv_proj_imports import *
from cv_proj_consts import *


def filename_label(o, **kwargs):
    "Label `item` with the file name."
    return o.name if isinstance(o, Path) else o.split(os.path.sep)[-1]

def find_best_lr(learn):
    learn.lr_find()
    
    cur_loss = None
    best_lr = None
    for i in range(len(learn.recorder.losses)):
        prev_loss = cur_loss
        cur_loss = learn.recorder.losses[i]
        if i==0: continue

        if cur_loss<prev_loss:
            best_lr = learn.recorder.lrs[i]
    
    return best_lr / 10.

def create_submission(image_files, predictions):
    with open('submission.txt', 'w+') as outfile:
        outfile.write('Image,Id\n')
        for (image,whale) in zip(image_files,predictions):
            whale_predictions = train_labels[whale]
            
            uniq_pred = list()
            for pred in whale_predictions:
                if len(uniq_pred)==5: break
                if pred not in uniq_pred:
                    uniq_pred.append(pred)                    
            
            if len(uniq_pred)<5:
                raise Exception("not enough predictions: " + str(whale_predictions))
            
            outfile.write(image.name+','+' '.join(uniq_pred)+'\n')
            
def test_submission(image_files, predictions):
    scores = []
    
    for (image,whale) in zip(image_files,predictions):
        whale_predictions = train_labels[whale]

        uniq_pred = list()
        for pred in whale_predictions:
            if len(uniq_pred)==5: break
            if pred not in uniq_pred:
                uniq_pred.append(pred)                    

        if len(uniq_pred)<5:
            raise Exception("not enough predictions: " + str(whale_predictions))

        target = train_labels_dict[image.name]
        
        map5 = 0
        if target in uniq_pred:
            map5 = 1/(1+uniq_pred.index(target))
        
        scores.append(map5)
        
    return np.mean(scores)