# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# https://ipython.org/ipython-doc/3/config/extensions/autoreload.html
# %load_ext autoreload
# %autoreload 2

from cv_proj_imports import *
from cv_proj_consts import *
from cv_proj_utils import *
from cv_proj_explore_map5_metric import *
from cv_proj_trainval_split import *
from cv_proj_arcface import *
import pickle
import bz2

train_image_files


# +
def filename_label(o, **kwargs):
    "Label `item` with the file name."
    return o.name if isinstance(o, Path) else o.split(os.path.sep)[-1]

def img_to_ftrs_fn(img_fn, **kwargs):
    return f'efficientnet_features/{img_fn}.pickle.bz2'

def load_features(fn):
    with bz2.BZ2File(fn, 'rb') as f:
        ftrs = pickle.load(f)        
        ftrs = ftrs.reshape([2560, 16, 16])
        return ftrs


# -

filename_label(train_image_files[0])

img_to_ftrs_fn(filename_label(train_image_files[0]))

load_features(img_to_ftrs_fn(filename_label(train_image_files[0])))

# +
# transformations
# we get the filenames as input, and output:
# x as the image feature vector
# y as the whale name
tfms_x = [filename_label, img_to_ftrs_fn, load_features]
tfms_y = [filename_label, lambda o: train_labels_dict[o], Categorize()]
tfms = [tfms_x, tfms_y]
ds_tfms = [ToTensor()]

dl_tfms = [Cuda()]
# -

trainval_splits = get_trainval_splits()
train_ds = DataSource(train_image_files, tfms, splits=trainval_splits)

train_db = train_ds.databunch(after_item=ds_tfms, after_batch=dl_tfms, bs=32, shuffle_train=True, num_workers=0)


class PassthroughNet(nn.Sequential):
    def __init__(self, pretrained=True, **kwargs):
        super().__init__()
    
    def forward(self, x):
        # reshape to original shape of EfficientNet-B7 features
        return x.reshape([2560, 16, 16])


# +
from fastai2.vision.learner import _resnet_split

opt_func = partial(Adam, lr=5e-3, wd=0.01)

arcface_loss = ArcFaceLoss().cuda() # this may not be needed just try it out
custom_head = Customhead(5120,1078,1024)

model = create_cnn_model(
    arch=PassthroughNet,
    nc = 1078,
    cut = -2,
    pretrained = True,
    custom_head=custom_head
)

learn = Learner(train_db, model, loss_func=arcface_loss, splitter = _resnet_split, opt_func=opt_func, metrics=[error_rate, mapk])
learn.unfreeze()
# -

learn.model

#chosen_lr = find_best_lr(learn); f"Chosen LR: {chosen_lr}"


# +
learn_cbs = [
    EarlyStoppingCallback(monitor='valid_loss', patience=6),
    SaveModelCallback(monitor='valid_loss', fname='embedding_efficientnetb7_20191031_arcface_best_v3'),
    ReduceLROnPlateau(monitor='valid_loss', patience=2)
]

#learn.fit(500, lr=chosen_lr, cbs=learn_cbs)
# -

learn.fit(5, lr=0.001, cbs=learn_cbs)
learn.recorder.plot_loss()

learn.fit(10, lr=0.0005, cbs=learn_cbs)
learn.recorder.plot_loss()

learn.fit(30, lr=0.00005, cbs=learn_cbs)
learn.recorder.plot_loss()

learn.fit(30, lr=0.000005, cbs=learn_cbs)
learn.recorder.plot_loss()
