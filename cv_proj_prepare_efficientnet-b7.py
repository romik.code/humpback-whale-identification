# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
# https://github.com/lukemelas/EfficientNet-PyTorch
# -

# https://ipython.org/ipython-doc/3/config/extensions/autoreload.html
# %load_ext autoreload
# %autoreload 2

# +
from PIL import Image
import torch
from torchvision import transforms
from efficientnet_pytorch import EfficientNet
from torch import nn
from cv_proj_consts import *
import bz2
import pickle

device = torch.device(torch.cuda.current_device())
#device = torch.device('cpu')

model = EfficientNet.from_pretrained(model_name='efficientnet-b7', num_classes=1000)
model.eval()
model = model.to(device)

#tfms = transforms.Compose([transforms.ToTensor(), transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),])
tfms = transforms.Compose([transforms.ToTensor(),])

train_image_paths = L(sorted(get_image_files(train_processed_data_dir)))
test_image_paths = L(sorted(get_image_files(test_processed_data_dir)))
image_paths = L(sorted(train_image_paths + test_image_paths))


# -

def load_image(fn):
    im = Image.open(fn)
    im = im.resize((512,512))
    im.load()
    im = im._new(im.im)
    im = im.convert('RGB')
    return im


model._fc.in_features

# +
bs = 16

image_count = len(image_paths)

ft_shape = None

for i in tqdm(range(int(image_count/bs)+1)):
    
    last_idx = (i+1)*bs
    if last_idx>image_count: last_idx=image_count
    image_paths_subset = image_paths[i*bs:last_idx]
    batch_image_count = len(image_paths_subset)

    with torch.no_grad():
    
        imgs = torch.cat([
            tfms(load_image(img_path)).unsqueeze(0).to(device)
            for img_path in image_paths_subset])

        img_names = [img.name for img in image_paths_subset]    
    
        ft = model.extract_features(imgs)        
        if ft_shape is None:
            ft_shape = ft.shape
            print(ft_shape)

        ft = ft.view(batch_image_count, -1)
    
        for j in range(batch_image_count):
            img_name = img_names[j]
            #ftrs = ft[j] #.tolist()
            ftrs = ft[j].cpu() #.numpy().tobytes()
            
            with bz2.BZ2File(f'efficientnet_features/{img_name}.pickle.bz2', 'wb') as f:
                pickle.dump(ftrs, f)
                #f.write(msgpack.packb(ftrs, use_bin_type=True))
            
            #assert len(ftrs)==1792
            #img_ftrs[img_name] = ftrs
