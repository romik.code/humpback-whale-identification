# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# https://ipython.org/ipython-doc/3/config/extensions/autoreload.html
# %load_ext autoreload
# %autoreload 2

from cv_proj_imports import *
from cv_proj_utils import *
from cv_proj_consts import *
from cv_proj_explore_map5_metric import *
from fastai2.vision.learner import _resnet_split
from torchvision import models

dl_tfms = [Cuda(), ByteToFloatTensor(), Normalize(*imagenet_stats)]
ds_img_tfms = [ToTensor(), Resize(224)]

train_tfms_x = [PILImage.create]
train_tfms = [train_tfms_x]
train_ds = DataSource(train_image_files, train_tfms)
train_db = train_ds.databunch(after_item=ds_img_tfms, after_batch=dl_tfms, shuffle_train=False, bs=64, num_workers=1)

# +
test_tfms_x = [PILImage.create]
test_tfms = [test_tfms_x]

test_ds = DataSource(test_image_files, test_tfms)
test_db = test_ds.databunch(after_item=ds_img_tfms, after_batch=dl_tfms, shuffle_train=False, bs=64, num_workers=1)

# +
opt_func = partial(Adam, lr=5e-3, wd=0.01)
train_learn = cnn_learner(train_db, models.resnet34, loss_func=CrossEntropyLossFlat(), cut=-2, splitter = _resnet_split, opt_func=opt_func, metrics=[error_rate, mapk])

train_learn.model

# +
# remove resnet34 last FC layer
del train_learn.model[1][8]

train_learn.load('embedding_resnet34_20191027_v2_unfreeze_best', strict=False)
# -

train_preds, _ = train_learn.get_preds(dl = train_db.dls[0], with_loss=False, decoded=False, act=noop)
(len(train_preds), len(train_preds[0]))

test_preds, _ = train_learn.get_preds(dl = test_db.dls[0], with_loss=False, decoded=False, act=noop)
(len(test_preds), len(test_preds[0]))

# https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.cdist.html
# PyTorch documentation is missing...
train_test_cdist = torch.cdist(test_preds, train_preds)

train_train_cdist = torch.cdist(train_preds, train_preds)

(len(train_test_cdist), len(train_test_cdist[0]))

(len(train_train_cdist), len(train_train_cdist[0]))

_, train_test_cdist_topk_pred = torch.topk(train_test_cdist, k=100, largest=False)

_, train_train_cdist_topk_pred = torch.topk(train_train_cdist, k=100, largest=False)

train_test_cdist_topk_pred

train_test_cdist_topk_pred

len(train_test_cdist_topk_pred)

len(train_train_cdist_topk_pred)


# +
# ToDo: measure the threshold for 'new_whale' and use that to compare

# +
# ToDo: find centroid for each whale and measure distance to that

# +
def create_submission(image_files, predictions):
    with open('submission.txt', 'w+') as outfile:
        outfile.write('Image,Id\n')
        for (image,whale) in zip(image_files,predictions):
            whale_predictions = train_labels[whale]
            
            uniq_pred = list()
            for pred in whale_predictions:
                if len(uniq_pred)==5: break
                if pred not in uniq_pred:
                    uniq_pred.append(pred)                    
            
            if len(uniq_pred)<5:
                raise Exception("not enough predictions: " + str(whale_predictions))
            
            outfile.write(image.name+','+' '.join(uniq_pred)+'\n')
            
def test_submission(image_files, predictions):
    scores = []
    
    for (image,whale) in zip(image_files,predictions):
        whale_predictions = train_labels[whale]

        uniq_pred = list()
        for pred in whale_predictions:
            if len(uniq_pred)==5: break
            if pred not in uniq_pred:
                uniq_pred.append(pred)                    

        if len(uniq_pred)<5:
            raise Exception("not enough predictions: " + str(whale_predictions))

        target = train_labels_dict[image.name]
        
        map5 = 0
        if target in uniq_pred:
            map5 = 1/(1+uniq_pred.index(target))
        
        scores.append(map5)
        
    return np.mean(scores)


# -

train_score = test_submission(train_image_files, train_train_cdist_topk_pred); train_score

create_submission(test_image_files, train_test_cdist_topk_pred)

# !kaggle competitions submit -f submission.txt -m "cdist_topk embedding_resnet34_20191027_v2_unfreeze_best" humpback-whale-identification
