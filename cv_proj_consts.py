from cv_proj_imports import *

data_dir = Path('/home/crazy_slime/projects/humpback-whale-identification/data')

# train
train_data_dir = data_dir/'train'
train_mask_data_dir = data_dir/'train_mask'
train_processed_data_dir = data_dir/'train_processed'
train_labels_path = data_dir/'train.csv'
train_labels_df = pd.read_csv(train_labels_path) 
train_labels_df.index.rename('rownum', inplace=True)

train_labels_dict = train_labels_df.set_index('Image').T.to_dict('records')[0]
train_labels_inv_dict = defaultdict(list)
{train_labels_inv_dict[v].append(k) for k, v in train_labels_dict.items()}

train_indices_no_new_whales = L(train_labels_df[train_labels_df["Id"]!='new_whale'].index.tolist())

train_image_files = L(sorted(get_image_files(train_processed_data_dir))) # sorted so the order matches labels
train_image_files_len = len(train_image_files)

train_labels = L(train_labels_df['Id'].tolist())

train_image_files_no_newwhale = train_image_files[train_indices_no_new_whales]
train_labels_no_newwhale = train_labels[train_indices_no_new_whales]

# test
test_data_dir = data_dir/'test'
test_mask_data_dir = data_dir/'test_mask'
test_processed_data_dir = data_dir/'test_processed'
test_image_files = L(sorted(get_image_files(test_processed_data_dir))) # sorted so the order matches labels
test_image_files_len = len(test_image_files)
