# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# https://ipython.org/ipython-doc/3/config/extensions/autoreload.html
# %load_ext autoreload
# %autoreload 2

from cv_proj_imports import *
from cv_proj_consts import *
from cv_proj_utils import *
from cv_proj_explore_map5_metric import *
from cv_proj_trainval_split import *
from cv_proj_arcface import *

# +
# transformations
# we get the filenames as input, and output:
# x as the image
# y as the whale name
tfms_x = [PILImage.create]
tfms_y = [filename_label, lambda o: train_labels_dict[o], Categorize()]
tfms = [tfms_x, tfms_y]

ds_img_tfms = [ToTensor(), Resize(224)]

dl_tfms = [Cuda(), ByteToFloatTensor(), Normalize(*imagenet_stats)]
dl_tfms.extend(aug_transforms(
    do_flip=False, # flipping makes it a different whale identity!
    flip_vert=False,
    max_rotate=30.,
    max_zoom=1.,
    max_lighting=0.4,
    max_warp=0.1,
    p_affine=0.75,
    p_lighting=0.75,
    pad_mode=PadMode.Border
))
# -

trainval_splits = get_trainval_splits()
train_ds = DataSource(train_image_files, tfms, splits=trainval_splits)
train_db = train_ds.databunch(after_item=ds_img_tfms, after_batch=dl_tfms, bs=32, num_workers=0)

# +
from fastai2.vision.learner import _resnet_split

opt_func = partial(Adam, lr=5e-3, wd=0.01)

arcface_loss = ArcFaceLoss().cuda() # this may not be needed just try it out
custom_head=Customhead(2048,1078)

model = create_cnn_model(
    arch=models.resnet34,
    nc = 1078,
    cut = -2,
    pretrained = True,
    custom_head=custom_head
)

learn = Learner(train_db, model, loss_func=arcface_loss, splitter = _resnet_split, opt_func=opt_func, metrics=[error_rate, mapk])
# -

chosen_lr = find_best_lr(learn); f"Chosen LR: {chosen_lr}"

# +
learn_cbs = [
    EarlyStoppingCallback(monitor='valid_loss', patience=20),
    SaveModelCallback(monitor='valid_loss', fname='embedding_resnet34_20191027_arcface_best'),
    ReduceLROnPlateau(monitor='valid_loss', patience=5)
]

learn.fit(500, lr=chosen_lr, cbs=learn_cbs)
# -

learn.recorder.plot_loss()

learn.unfreeze()

chosen_lr = find_best_lr(learn); f"Chosen LR: {chosen_lr}"

# +
learn_cbs = [
    EarlyStoppingCallback(monitor='valid_loss', patience=20),
    SaveModelCallback(monitor='valid_loss', fname='embedding_resnet34_20191027_arcface_unfreeze_best'),
    ReduceLROnPlateau(monitor='valid_loss', patience=5)
]

learn.fit(500, lr=chosen_lr, cbs=learn_cbs)
